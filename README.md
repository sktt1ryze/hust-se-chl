# 华中科技大学软件工程课设

## 程序员给爷爬队
队伍成员：  
+ [车春池](https://gitee.com/sktt1ryze/)（项目管理员，负责项目主体框架的设计，后端功能实现，代码贡献量 40%）
+ [郝凯欣](https://gitee.com/hao-kaixin31)（项目开发者，负责音频处理，曲谱设计和解析，代码贡献量 30%）
+ [李家伟](https://gitee.com/kirito_602)（项目开发者，负责 TUI 界面设计，图形渲染，代码贡献量 30%）

## rust-piano
`Rust` 语言实现的命令行虚拟钢琴  
目前已经在`github`上开源：[rust-piano](https://github.com/SKTT1Ryze/rust-piano)  
```
        _  .-')                  .-')     .-') _    
        ( \( -O )                ( OO ).  (  OO) )   
        ,------.   ,--. ,--.   (_)---\_) /     '._  
        |   /`. '  |  | |  |   /    _ |  |'--...__) 
        |  /  | |  |  | | .-') \  :` `.  '--.  .--' 
        |  |_.' |  |  |_|( OO ) '..`''.)    |  |    
        |  .  '.'  |  | | `-' /.-._)   \    |  |    
        |  |\  \  ('  '-'(_.-' \       /    |  |    
        `--' '--'   `-----'     `-----'     `--'
        PPPPPPPPPPPPPPPPP     iiii                                                       
        P::::::::::::::::P   i::::i                                                      
        P::::::PPPPPP:::::P   iiii                                                       
        PP:::::P     P:::::P                                                             
        P::::P     P:::::Piiiiiii   aaaaaaaaaaaaa   nnnn  nnnnnnnn       ooooooooooo   
        P::::P     P:::::Pi:::::i   a::::::::::::a  n:::nn::::::::nn   oo:::::::::::oo 
        P::::PPPPPP:::::P  i::::i   aaaaaaaaa:::::a n::::::::::::::nn o:::::::::::::::o
        P:::::::::::::PP   i::::i            a::::a nn:::::::::::::::no:::::ooooo:::::o
        P::::PPPPPPPPP     i::::i     aaaaaaa:::::a   n:::::nnnn:::::no::::o     o::::o
        P::::P             i::::i   aa::::::::::::a   n::::n    n::::no::::o     o::::o
        P::::P             i::::i  a::::aaaa::::::a   n::::n    n::::no::::o     o::::o
        P::::P             i::::i a::::a    a:::::a   n::::n    n::::no::::o     o::::o
        PP::::::PP         i::::::ia::::a    a:::::a   n::::n    n::::no:::::ooooo:::::o
        P::::::::P         i::::::ia:::::aaaa::::::a   n::::n    n::::no:::::::::::::::o
        P::::::::P         i::::::i a::::::::::aa:::a  n::::n    n::::n oo:::::::::::oo 
        PPPPPPPPPP         iiiiiiii  aaaaaaaaaa  aaaa  nnnnnn    nnnnnn   ooooooooooo   
```
## 功能
+ 音乐播放列表
+ 虚拟钢琴
+ 自动播放曲子

## 编译依赖
+ Rust 语言编译链。  

### 安装 Rust 环境
#### Linux
```
$ curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
```
#### Windows
前往[rust-win](https://www.rust-lang.org/tools/install)安装 Rust。建议使用 Linux。  

### Rust 更换清华源
编辑`~/.cargo/config`文件，添加以下内容
```
[source.crates-io]
replace-with = 'tuna'

[source.tuna]
registry = "https://mirrors.tuna.tsinghua.edu.cn/git/crates.io-index.git"
```

## 运行项目
### 下载项目
```
git clone https://gitee.com/sktt1ryze/hust-se-chl.git
```
### 运行
```
cd hust-se-chl
cd rust-piano
cargo run
```

## Demo 演示
<img src = "./img/run_0.png" width = "80%">  
<img src = "./img/run_1.png" width = "80%">  

## 开源协议
[MIT 开源协议](./LICENSE)  



